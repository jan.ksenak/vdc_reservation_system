<?php


class simpleJWTAuth {
    
    

    private $header = null;

    private $payload = null;
        

    private function base64UrlEncode($text)
    {
        return str_replace(
            ['+', '/', '='],
            ['-', '_', ''],
            base64_encode($text)
        );
    }

    private function createPayload($user_id){
        $date   = new DateTimeImmutable();

        $this->payload = json_encode([
            'iat' => $date->getTimestamp(),
            'iss' => 'your.domain.name',
            'exp' => $date->modify('+1 day')->getTimestamp(),
            'user_id' => $user_id
        ]);
    }

    public function createJWTToken($user_id){

        $this->header = json_encode([
            'typ' => 'JWT',
            'alg' => 'HS256'
        ]);

        $this->createPayload($user_id);
        

        $base64UrlHeader = $this->base64UrlEncode($this->header);

        $base64UrlPayload = $this->base64UrlEncode($this->payload);

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, SECRET, true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = $this->base64UrlEncode($signature);

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
        return $jwt;
    }


    public function validateJWTToken($jwt_token){
        if (! isset($jwt_token)) {
            return;
        }

        $date = new DateTimeImmutable();

        $tokenParts = explode('.', $jwt_token);
        $header = base64_decode($tokenParts[0]);
        $payload = base64_decode($tokenParts[1]);
        $payload_data = json_decode($payload,true);
        $signatureProvided = $tokenParts[2];
        
        
        $expiration = $payload_data['exp'];
        $tokenExpired = ($date->getTimestamp() >= $expiration);
       
        // build a signature based on the header and payload using the secret
        $base64UrlHeader = $this->base64UrlEncode($header);
        $base64UrlPayload = $this->base64UrlEncode($payload);
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, SECRET, true);
        $base64UrlSignature = $this->base64UrlEncode($signature);

        // verify it matches the signature provided in the token
        $signatureValid = ($base64UrlSignature === $signatureProvided);

        return !$tokenExpired && $signatureValid;

        
    }
}