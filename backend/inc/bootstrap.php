

<?php
define("PROJECT_ROOT_PATH", __DIR__ . "\\..\\");

require_once PROJECT_ROOT_PATH . "/inc/config.php";

require_once PROJECT_ROOT_PATH . "/router.php";

require_once PROJECT_ROOT_PATH . "/simpleJWTAuth.php";


require_once PROJECT_ROOT_PATH . "/Models/Database.php";

require_once PROJECT_ROOT_PATH . "/Models/UserModel.php";

require_once PROJECT_ROOT_PATH . "/Models/RoomModel.php";

require_once PROJECT_ROOT_PATH . "/Models/ReservationModel.php";

require_once PROJECT_ROOT_PATH . "/Models/LectionModel.php";

require_once PROJECT_ROOT_PATH . "/Models/carsModel.php";

require_once PROJECT_ROOT_PATH . "Controllers/BaseController.php";

require_once PROJECT_ROOT_PATH . "Controllers/UserController.php";

require_once PROJECT_ROOT_PATH . "Controllers/RoomController.php";

require_once PROJECT_ROOT_PATH . "Controllers/ReservationController.php";

require_once PROJECT_ROOT_PATH . "Controllers/LectionController.php";

require_once PROJECT_ROOT_PATH . "/Controllers/carsController.php";