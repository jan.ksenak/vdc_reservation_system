<?php

namespace Controllers;

use Models\LectionModel;
use Exception;

    class LectionController extends BaseController{

        private $lection_model = null;

        public function __construct(){
            $this->lection_model = new LectionModel();
        }

        public function getClientLectionByRoomIdByDate($username,$room_id,$date,$jwt_token) {

            if(!$this->GetisAuthorized($jwt_token)){
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                return;
            }

            $strErrorHeader = "";

            try{
                $lections = $this->lection_model->getClientLectionByRoomIdByDate($username,$room_id, $date);
                
                $responseData = json_encode($lections);
            }catch(Exception $e) {
                $strErrorDesc = $e->getMessage();
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';  
            }

            if(!$strErrorHeader){
                $this->sendRequest($responseData, array("Content-Type: application/json", "HTTP/1.1 200 OK"));
            }else{
                $this->sendRequest(array('error'=> $strErrorDesc), array("Content-Type: application/json", $strErrorHeader));
            }
        }

        public function getLectionByUsername($username,$jwt_token){
            if(!$this->GetIsAuthorized($jwt_token)){
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                return;
            }

            $strErrorHeader = "";

            try{
                $lections = $this->lection_model->getLectionByUsername($username);
                
                $responseData = json_encode($lections);
            }catch(Exception $e) {
                $strErrorDesc = $e->getMessage();
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';  
            }

            if(!$strErrorHeader){
                $this->sendRequest($responseData, array("Content-Type: application/json", "HTTP/1.1 200 OK"));
            }else{
                $this->sendRequest(array('error'=> $strErrorDesc), array("Content-Type: application/json", $strErrorHeader));
            }
        }


        public function createLection($username) {
            if(!$this->isAuthorized()){
                
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                return;
            }

            $data = json_decode(file_get_contents('php://input'), true);

            if (json_last_error() === JSON_ERROR_NONE){
                $this->lection_model->createLection($username,$data['params']['room_id'],$data['params']['type'],$data['params']['start_time'],$data['params']['end_time']);
            }            
        }
    
        public function deleteLection($username,$lection_id,$jwt_token) {

            if(!$this->GetIsAuthorized($jwt_token)){
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                return;
            }

            $this->lection_model->deleteLection($username,$lection_id);
        }


        public function getLectionTypes($jwt_token){
            if(!$this->GetIsAuthorized($jwt_token)){
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                return;
            }

            $strErrorHeader = "";

            try{
                $lections = $this->lection_model->getLectionTypes();
                $responseData = json_encode($lections);
            }catch(Exception $e) {
                $strErrorDesc = $e->getMessage();
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';  
            }

            if(!$strErrorHeader){
                $this->sendRequest($responseData, array("Content-Type: application/json", "HTTP/1.1 200 OK"));
            }else{
                $this->sendRequest(array('error'=> $strErrorDesc), array("Content-Type: application/json", $strErrorHeader));
            }
        }


        
    }
?>