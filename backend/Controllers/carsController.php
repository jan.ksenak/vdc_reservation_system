<?php

namespace Controllers;

use Models\carsModel;
use Exception;

class carsController extends BaseController{

        private $cars_model = null;

        public function __construct(){
            $this->cars_model = new carsModel();
        }

        public function getCars(){
            $cars = $this->cars_model->getAll();
            //print_r($cars);

            return $cars;
            
        }

    }
?>