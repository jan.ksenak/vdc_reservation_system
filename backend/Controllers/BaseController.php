<?php

namespace Controllers;

    class BaseController{

        public function sendRequest($data, $httpHeaders=array()) {
            //TODO Maybe delete in future
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
            header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
            header_remove('Set-Cookie');
            if (is_array($httpHeaders) && count($httpHeaders)) {
                foreach ($httpHeaders as $httpHeader) {
                    header($httpHeader);
                }
            }
            echo $data;
            exit;
        }

        public function GetIsAuthorized($jwt_token){
            
            $token_is_valid=false;
            
           if (isset($jwt_token)) {
                
                $jwtAuth = new \simpleJWTAuth();
                $token_is_valid = $jwtAuth->validateJWTToken($jwt_token);
                    
            }
            return $token_is_valid;   
        }


        public function isAuthorized(){
            $jwt_token = null;
            $token_is_valid=false;
            $data = json_decode(file_get_contents('php://input'), true);
            
            if (json_last_error() !== JSON_ERROR_NONE){
                    return;
                }
           
            
           if (isset($data['params']['jwt_token'])) {
                $jwt_token = $data['params']['jwt_token'];
                $jwtAuth = new \simpleJWTAuth();
                $token_is_valid = $jwtAuth->validateJWTToken($jwt_token);
                
                    
            }
            
    
            return $token_is_valid;   
        }
        
        // public function isAuthorized(){
        //     $jwt_token = null;
        //     $token_is_valid=false;
        //     $httpHeaders = apache_request_headers();
            
        //     if (isset($httpHeaders['Authorization'])) {
        //         $jwt_token = $this->getBearerToken($httpHeaders['Authorization']);
                
        //         $jwtAuth = new \simpleJWTAuth();
        //         $token_is_valid = $jwtAuth->validateJWTToken($jwt_token);
        //     }

        //     if(!$token_is_valid){
        //         $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
        //     }

        //     return $token_is_valid; 
             
        // }

        // private function getBearerToken($baererToken) {
            
        //     // HEADER: Get the access token from the header
        //     if (!empty($baererToken)) {
        //         if (preg_match('/Bearer\s((.*)\.(.*)\.(.*))/', $baererToken, $matches)) {
        //             return $matches[1];
        //         }
        //     }
        //     return null;
        // }

        
    }
?>