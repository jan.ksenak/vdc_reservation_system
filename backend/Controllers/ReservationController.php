<?php

namespace Controllers;

use Models\ReservationModel;
use Exception;

    class ReservationController extends BaseController{

        private $reservation_model = null;

        public function __construct(){
            $this->reservation_model = new ReservationModel();
        }

        public function getReservationByUsername($username,$jwt_token) {

            if(!$this->GetIsAuthorized($jwt_token)){
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                return;
            }

            $strErrorHeader = "";

            try{
                $rooms = $this->reservation_model->getReservationByUsername($username);
                $responseData = json_encode($rooms);
            }catch(Exception $e) {
                $strErrorDesc = $e->getMessage();
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';  
            }

            if(!$strErrorHeader){
                $this->sendRequest($responseData, array("Content-Type: application/json", "HTTP/1.1 200 OK"));
            }else{
                $this->sendRequest(array('error'=> $strErrorDesc), array("Content-Type: application/json", $strErrorHeader));
            }
        }

        public function createReservation($username) {
            if(!$this->isAuthorized()){
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                return;
            }

            $data = json_decode(file_get_contents('php://input'), true);
            
            if (json_last_error() !== JSON_ERROR_NONE){
                return;
            }
        
            $this->reservation_model->createReservation($username,$data['params']['lection_id']);
            
        }

        public function deleteReservation($userId,$reservation_id,$jwt_token) {
            if(!$this->GetIsAuthorized($jwt_token)){
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                return;
            }

            $this->reservation_model->deleteReservation($userId,$reservation_id);
        }

        
    }
?>