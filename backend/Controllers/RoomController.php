<?php

namespace Controllers;

use Models\RoomModel;
use Exception;

    class RoomController extends BaseController{

        private $room_model = null;

        public function __construct(){
            $this->room_model = new RoomModel();
        }

        public function getAll($jwt_token) {

            $strErrorHeader = "";

             if(!$this->GetIsAuthorized($jwt_token)){
                $this->sendRequest(json_encode([]), array("Content-Type: application/json", "HTTP/1.1 401 Unauthorized"));
                  return;
             }
            
            try{
                $rooms = $this->room_model->getAll();
                $responseData = json_encode($rooms);
            }catch(Exception $e) {
                $strErrorDesc = $e->getMessage();
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';  
            }

            if(!$strErrorHeader){
                $this->sendRequest($responseData, array("Content-Type: application/json", "HTTP/1.1 200 OK"));
            }else{
                $this->sendRequest(array('error'=> $strErrorDesc), array("Content-Type: application/json", $strErrorHeader));
            }
        }

        
    }
?>