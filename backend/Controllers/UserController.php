<?php

namespace Controllers;

use Exception;
use Models\UserModel;

    class UserController extends BaseController{


        private $user_model = null;

        public function __construct(){
            $this->user_model = new UserModel();
        }


        public function getUserById($user_id,$jwt_token) {

             if(!$this->GetIsAuthorized($jwt_token)){
                 return;
             }

            $strErrorHeader = "";
            
            try{
                $users = $this->user_model->getUserById($user_id);
                
                $responseData = json_encode($users);
            }catch(Exception $e) {
                $strErrorDesc = $e->getMessage();
                $strErrorHeader = 'HTTP/1.1 500 Internal Server Error';  
            }

            if(!$strErrorHeader){
                $this->sendRequest($responseData, array("Content-Type: application/json", "HTTP/1.1 200 OK"));
            }else{
                $this->sendRequest(array('error'=> $strErrorDesc), array("Content-Type: application/json", $strErrorHeader));
            }
            
        }

        public function registerUser() {
            $data = json_decode(file_get_contents('php://input'), true);
            
            if (json_last_error() !== JSON_ERROR_NONE){
                return;
            }
            $response = $this->user_model->registerUser($data['params']['email'],$data['params']['username'],$data['params']['password']);
            
            if($response['status']===200){
                $jwtAuth = new \simpleJWTAuth();
                $jwt_token = $jwtAuth->createJWTToken($response['user_id']);
                $response['jwt_token'] = $jwt_token;
                $response = json_encode($response);
                $this->sendRequest($response, array("Content-Type: application/json", "HTTP/1.1 200 OK", "Authorization: Bearer " .$jwt_token));
            }else{
                $response = json_encode($response);
                $this->sendRequest($response, array("Content-Type: application/json", "HTTP/1.1 500 Internal Server Error"));
            }

            
        }

        public function loginUser() {
            $data = json_decode(file_get_contents('php://input'), true);
            
            if (json_last_error() !== JSON_ERROR_NONE){
                return;
            }
            print_r($data);
            $response = $this->user_model->loginUser($data['email'],$data['password']);
            if($response['status']===200){
                $jwtAuth = new \simpleJWTAuth();
                $jwt_token = $jwtAuth->createJWTToken($response['user_id']);
                $response['jwt_token'] = $jwt_token;
                $response = json_encode($response);
                $this->sendRequest($response, array("Content-Type: application/json", "HTTP/1.1 200 OK"));
            }else{
                $response = json_encode($response);
                $this->sendRequest($response, array("Content-Type: application/json", "HTTP/1.1 500 Internal Server Error"));
            }
        }
    }
?>