<?php
require __DIR__ . "\\inc\\bootstrap.php";

use Controllers\carsController;

$RoomController = new carsController();
$cars = $RoomController->getCars();
print_r($cars);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Website</title>
    <!-- <link rel="stylesheet" href="styles.css"> -->
</head>
<body>
    <main>
        <section>
            <h2>Title</h2>
            <p>This is a brief description about our website.</p>
        </section>

        <label for="cars">Choose a car:</label>
        <?php
                foreach ($cars as $key => $value) {
                    
                    echo "{$key} => {$value} ";
                   
                }
            ?>
        <select name="cars" id="cars">
            <?php
                foreach ($cars as $key => $value) {
                    // $arr[3] will be updated with each value from $arr...
                    echo "{$key} => {$value[0] }";
                    
                }
            ?>
            <option value="volvo">Volvo</option>
            <option value="saab">Saab</option>
            <option value="mercedes">Mercedes</option>
            <option value="audi">Audi</option>
        </select>

        
</body>
</html>
