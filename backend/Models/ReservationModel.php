<?php




namespace Models;



use Models\Database;

class ReservationModel extends Database{
    
    public function getReservationByUsername($username)
    {
        return $this->select("SELECT reservation.id, lection_type.name, room.name AS room_name,CONCAT(trainer.firstname, ' ' ,trainer.lastname) AS trainer_name,lection_type.description, lection.start_time, lection.end_time FROM reservation
        INNER JOIN client_has_reservation ON reservation.id = client_has_reservation.reservation_id
        INNER JOIN client ON client_has_reservation.client_id = client.id
        INNER JOIN lection ON reservation.lection_id = lection.id
        INNER JOIN lection_type ON lection.type = lection_type.id
        INNER JOIN room ON lection.room_id = room.id
        INNER JOIN trainer ON lection.trainer_id = trainer.id
        INNER JOIN user ON client.user_id = user.id
        WHERE user.username=?",["s", [$username]]);
    }


    

    public function createReservation($username, $lection_id){
        
        $last_reservation_id = $this->insert("INSERT INTO reservation (lection_id,create_time) VALUES(?, NOW());",["i",[$lection_id]]);
        
        $this->createClientHasReservation($username,$last_reservation_id);

        $lection = new LectionModel();
        $lection->incrementLenctionById($lection_id);

        
    }

    private function createClientHasReservation($username,$reservation_id){

        
        $this->insert("INSERT INTO client_has_reservation 
                        VALUES ((SELECT client.id FROM client 
                                INNER JOIN user ON client.user_id = user.id
                                WHERE user.username = ?),?)",["si",[$username, $reservation_id]]);
    }

    public function getReservationById($reservation_id){
        return $this->select("SELECT id, lection_id FROM reservation WHERE id=? LIMIT 1",["i",[$reservation_id]]);
    }

    private function deleteClientHasReservation($username, $lection_id){
        $this->delete("DELETE c FROM client_has_reservation c
                        INNER JOIN client ON client_id=client.id
                        INNER JOIN user ON client.user_id = user.id
                        INNER JOIN reservation on reservation_id=reservation.id
                        INNER JOIN lection on lection_id=lection.id
                        WHERE user.username=? AND lection.id=?",["si",[$username, $lection_id]]);
    }

    public function deleteReservation($username,$reservation_id){
        $reservation = $this->getReservationById($reservation_id);
        $lection_id = $reservation[0]['lection_id'];
        $reservation_id = $reservation[0]['id'];
        $this->deleteClientHasReservation($username,$lection_id);

        $this->delete("DELETE FROM reservation WHERE id=?",["i",[$reservation_id]]);
        
        $lection = new LectionModel();
        $lection->decrementLenctionById($lection_id);

    }

    
   
}