<?php




namespace Models;


use Exception;
use Models\Database;

class UserModel extends Database{
    
    public function getUsers()
    {
        
        return $this->select("SELECT * FROM user");
    }

    public function getUserById($user_id)
    {
        
         return $this->select("SELECT username, user_role.name AS 'role' FROM user 
                             INNER JOIN user_role ON user.role_id=user_role.id
                             WHERE user.id =?", ["i",[$user_id]]);
    }

    public function getUserByEmail($email)
    {
        return $this->select("SELECT * FROM user WHERE email=? LIMIT 1", ['s',[$email]]);
    }

    public function getUserByUsername($username)
    {
        return $this->select("SELECT * FROM user WHERE username=? LIMIT 1", ['s',[$username]]);
    }

    public function createClient($user_id){
        $this->insert("INSERT INTO client(user_id) VALUES (?)", ['i',[$user_id]]);
    }

    public function createTrainer($user_id){
        $this->insert("INSERT INTO trainer(user_id) VALUES (?)", ['i',[$user_id]]);
    }

    private function createUser($email,$username,$password,$user_role=1)
    {
        $user_id = $this->insert("INSERT INTO user(username,password,email,role_id) VALUES (?,?,?,?)", ['sssi',[$username,$password,$email,$user_role]]);
        if($user_role===1){
            $this->createClient($user_id);
        }elseif($user_role===2){
            $this->createTrainer($user_id);
        }
        
    }

    public function registerUser($email,$username,$password)
    {

            $response = [
                'message' => "",
                'status' => 500,
                'user_id' => null
                ];

            $email = $this->connection->real_escape_string($email);
            $username = $this->connection->real_escape_string($username);
            $password = $this->connection->real_escape_string($password);
            $password = password_hash($password,PASSWORD_DEFAULT);

            $user_exists = $this->getUserByEmail($email);
            $username_exists = $this->getUserByUsername($username);
            

            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $response['message'] = "Invalid E-mail adrress";
            }else if($user_exists){
                $response['message'] = "This Email is already exists";
            }else if($username_exists){
                    $response['message'] = "This username is already exists";
            }else if(strlen($username) < 6){
                $response['message'] = "Username too short";
            }else if(strlen($password) < 6){
                $response['message'] = "Password too short";
            }else{
                $response['status'] = 200;
                $response['message'] = "Success";
            }

            if($response['status'] !== 200){
                return $response;
            }
            
            try{
                $this->createUser($email,$username,$password);
                $user = $this->getUserByEmail($email);
               
                $response['user_id'] = $user[0]['id'];
                
            }catch(Exception $e){
                $response['status'] = 500;
                $response['message'] = "Registration Failed, please try agian";
                return $response;
            }
            
            return $response;
    }

    public function loginUser($email,$password)
    {
        $response = [
            'message' => "",
            'status' => 500,
            'user_id'=> null
            ];
        
        $email = $this->connection->real_escape_string($email);
        $password = $this->connection->real_escape_string($password);
        
        $user= $this->getUserByEmail($email);

        if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
            $response['message'] = "Invalid E-mail adrress";
        }else if(!count($user)){
            $response['message'] = "User not found please create account";
        }else if(!password_verify($password,$user[0]['password'])){
            $response['message'] = "Wrong password";
        }else{
            $response['status'] = 200;
            $response['message'] = "Success";
            $response['user_id'] = $user[0]['id'];
        }

        
        
        return $response;
        }
}