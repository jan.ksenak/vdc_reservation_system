<?php




namespace Models;




use Models\Database;

class LectionModel extends Database{
    




    public function getLectionTypes(){
        return $this->select("SELECT * FROM lection_type");
    }
    
    
    public function getClientLectionByRoomIdByDate($username,$room_id, $date)
    {
        
        return $this->select(
        "SELECT lection.id,lection_type.name, room.name AS room_name, CONCAT(trainer.firstname, ' ' ,trainer.lastname) AS tranier_name, 
        lection.clients, lection_type.max_capacity, lection.start_time,lection.end_time
        FROM lection
        INNER JOIN room ON lection.room_id = room.id
        INNER JOIN trainer ON lection.trainer_id = trainer.id
        INNER JOIN lection_type ON lection.type = lection_type.id
        WHERE room.id=? AND DATE_FORMAT(lection.start_time, '%Y-%m-%d')=? AND lection.id NOT IN(
                    SELECT lection.id FROM lection
                    INNER JOIN reservation ON reservation.lection_id=lection.id
                    INNER JOIN client_has_reservation ON reservation.id=client_has_reservation.reservation_id
                    INNER JOIN client ON client.id=client_has_reservation.client_id
                    INNER JOIN user ON client.user_id=user.id
                    WHERE username =?)", ["iss",[$room_id, $date,$username]]);
    }

    public function getLectionByUsername($username)
    {
        
        return $this->select(
        "SELECT lection.id,lection_type.name, room.name AS room_name, lection_type.description, 
        lection.clients, lection_type.max_capacity, lection.start_time,lection.end_time
        FROM lection
        INNER JOIN room ON lection.room_id = room.id
        INNER JOIN trainer ON lection.trainer_id = trainer.id
        INNER JOIN user ON trainer.user_id = user.id
        INNER JOIN lection_type ON lection.type = lection_type.id
        WHERE user.username=? ", ["s", [$username]]);
    }

    public function incrementLenctionById($lection_id){
        $this->update("UPDATE lection SET clients=clients+1 WHERE lection.id = ?", ["i", [$lection_id]]);
    }

    public function decrementLenctionById($lection_id){
        $this->update("UPDATE lection SET clients=clients-1 WHERE lection.id = ?", ["i", [$lection_id]]);
    }

    public function createLection($username,$roomId,$lectionType,$startTime,$endTime) {
        $this->insert("INSERT INTO lection (trainer_id, room_id, start_time, end_time, type) 
        VALUES ((SELECT trainer.id FROM trainer 
                INNER JOIN user ON trainer.user_id = user.id
                WHERE user.username=?), ?,?,?,?)",["sissi",[$username,$roomId,$startTime,$endTime,$lectionType]]);
    }

    public function deleteLection($username,$lection_id) {
        
        $this->delete("DELETE l FROM lection l
                        INNER JOIN trainer ON trainer_id=trainer.id
                        INNER JOIN user ON trainer.user_id = user.id
                        WHERE user.username=? AND l.id=?",["si",[$username, $lection_id]]);
    }
   
}

