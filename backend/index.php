<?php




require __DIR__ . "\\inc\\bootstrap.php";
use Controllers\UserController;
use Controllers\RoomController;
use Controllers\ReservationController;
use Controllers\LectionController;




    $router = new Router();

    $router->addRoute('GET', BASE_URL.'rooms/:jwt_token', function ($jwt_token) {

        $RoomController = new RoomController();
        $RoomController->getAll($jwt_token);
        exit;
    });



    $router->addRoute('GET', BASE_URL.'lection_types/:jwt_token', function ($jwt_token) {

        $LectionController = new LectionController();
        $LectionController->getLectionTypes($jwt_token);
        exit;
    });


    $router->addRoute('GET', BASE_URL.':username/lections/:room_id/:date/:jwt_token', function ($username,$room_id,$date,$jwt_token) {

        $LectionController = new LectionController();
        $LectionController->getClientLectionByRoomIdByDate($username,$room_id,$date,$jwt_token);
        exit;
    });

    $router->addRoute('GET', BASE_URL.':username/lections/:jwt_token', function ($username,$jwt_token) {

        $LectionController = new LectionController();
        $LectionController->getLectionByUsername($username,$jwt_token);
        exit;
    });


    $router->addRoute('POST', BASE_URL.':username/lections', function ($username) {

        $LectionController = new LectionController();
        $LectionController->createLection($username);
        exit;
    });

    $router->addRoute('DELETE', BASE_URL.':username/lections/:lection_id/:jwt_token', function ($username, $lection_id,$jwt_token) {

        $LectionController = new LectionController();
        $LectionController->deleteLection($username, $lection_id,$jwt_token);
        exit;
    });

    $router->addRoute('GET', BASE_URL.':username/reservations/:jwt_token', function ($username,$jwt_token) {
        $ReservationController = new ReservationController();
        $ReservationController->getReservationByUsername($username,$jwt_token);
        exit;
    });

    $router->addRoute('POST', BASE_URL.':username/reservations', function ($username) {
        $ReservationController = new ReservationController();
        $ReservationController->createReservation($username);
        exit;
    });

    $router->addRoute('DELETE', BASE_URL.':username/reservations/:reservation_id/:jwt_token', function ($username,$reservation_id,$jwt_token) {
        $ReservationController = new ReservationController();
        $ReservationController->deleteReservation($username,$reservation_id,$jwt_token);
        exit;
    });

    $router->addRoute('GET', BASE_URL.'user/:user_id/:jwt_token', function ($user_id,$jwt_token) {
        $UserController = new UserController();
        $UserController->getUserById($user_id,$jwt_token);
        exit;
    });


    $router->addRoute('POST', BASE_URL.'login', function () {
        
        $UserController = new UserController();
        $UserController->loginUser();
        exit;
    });

    $router->addRoute('POST', BASE_URL.'register', function () {
        $UserController = new UserController();
        $UserController->registerUser();
        exit;
    });

    $router->matchRoute();

  

?>



