-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 17, 2024 at 05:08 PM
-- Server version: 8.0.18
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `m217372`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `firstname`, `lastname`, `user_id`) VALUES
(1, 'Karel', 'Čapek', 22),
(3, 'Uhorka', 'Zelená', 23);

-- --------------------------------------------------------

--
-- Table structure for table `client_has_reservation`
--

CREATE TABLE `client_has_reservation` (
  `client_id` int(11) UNSIGNED NOT NULL,
  `reservation_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client_has_reservation`
--

INSERT INTO `client_has_reservation` (`client_id`, `reservation_id`) VALUES
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(3, 36),
(3, 37),
(3, 38),
(3, 39),
(3, 44);

-- --------------------------------------------------------

--
-- Table structure for table `lection`
--

CREATE TABLE `lection` (
  `id` int(11) UNSIGNED NOT NULL,
  `trainer_id` int(11) UNSIGNED NOT NULL,
  `room_id` int(11) UNSIGNED NOT NULL,
  `clients` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lection`
--

INSERT INTO `lection` (`id`, `trainer_id`, `room_id`, `clients`, `start_time`, `end_time`, `type`) VALUES
(1, 1, 0, 0, '2024-04-10 18:00:00', '2024-04-10 19:00:00', 1),
(2, 1, 0, 0, '2024-04-17 18:00:00', '2024-04-17 19:00:00', 1),
(3, 1, 2, 0, '2024-04-16 08:00:00', '2024-04-16 09:00:00', 2),
(4, 1, 1, 1, '2024-04-23 16:00:00', '2024-04-23 18:00:00', 3),
(5, 1, 2, 2, '2024-04-23 08:00:00', '2024-04-23 09:00:00', 2),
(6, 1, 1, 1, '2024-04-30 16:00:00', '2024-04-30 18:00:00', 2),
(7, 1, 3, 0, '2024-04-07 12:00:00', '2024-04-07 13:00:00', 4),
(8, 1, 2, 0, '2024-04-13 09:00:00', '2024-04-13 13:00:00', 5),
(9, 1, 2, 1, '2024-04-26 09:00:00', '2024-04-26 13:00:00', 5),
(12, 1, 0, 0, '2024-04-10 19:00:00', '2024-04-10 20:00:00', 1),
(13, 1, 0, 0, '2024-04-14 19:00:00', '2024-04-14 20:00:00', 1),
(14, 1, 2, 0, '2024-04-10 10:00:00', '2024-04-10 12:00:00', 1),
(15, 1, 1, 0, '2024-04-11 10:00:00', '2024-04-11 12:00:00', 1),
(16, 1, 0, 0, '2024-04-10 00:00:00', '2024-04-10 12:00:00', 1),
(17, 1, 0, 0, '2024-04-10 10:00:00', '2024-04-10 12:00:00', 1),
(18, 1, 1, 0, '2024-04-10 10:00:00', '2024-04-10 11:00:00', 1),
(19, 1, 3, 0, '2024-04-11 11:00:00', '2024-04-11 13:00:00', 2),
(20, 1, 1, 0, '2024-04-13 16:00:00', '2024-04-13 17:00:00', 3),
(21, 1, 0, 0, '2024-04-12 13:00:00', '2024-04-12 14:00:00', 4),
(28, 1, 1, 0, '2024-04-15 12:00:00', '2024-04-15 13:00:00', 5),
(29, 3, 1, 0, '2024-04-17 10:00:00', '2024-04-17 11:00:00', 3),
(30, 3, 3, 1, '2024-04-18 11:00:00', '2024-04-18 14:00:00', 1),
(31, 3, 0, 2, '2024-04-18 09:00:00', '2024-04-18 10:00:00', 3),
(32, 3, 2, 0, '2024-04-20 12:00:00', '2024-04-20 13:00:00', 4),
(33, 3, 0, 1, '2024-04-19 14:00:00', '2024-04-19 15:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lection_type`
--

CREATE TABLE `lection_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `max_capacity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lection_type`
--

INSERT INTO `lection_type` (`id`, `name`, `description`, `max_capacity`) VALUES
(1, 'HIIT cvičenie', 'Intenzívny intervalový tréning pre posilnenie srdc...', 20),
(2, 'Jogging v parku', 'Relaxačný beh v prírode určený na zlepšenie vytrvalosti a kondície.', 20),
(3, 'Powerlifting workshop', 'Workshop zameraný na zdvíhanie ťažkých váh s dôrazom na techniku a svalový rozvoj.', 10),
(4, 'Jóga pre začiatočníkov', 'Lekcia jógy so zameraním na relaxáciu, dýchanie a zlepšenie flexibility.', 12),
(5, 'Bootcamp cvičenie', ' Intenzívny vonkajší tréningový program zahrňujúci rôzne cvičenia na celé telo.', 20);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) UNSIGNED NOT NULL,
  `lection_id` int(11) UNSIGNED NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `lection_id`, `create_time`, `update_time`) VALUES
(36, 31, '2024-04-17 18:54:39', '2024-04-17 18:54:39'),
(37, 30, '2024-04-17 18:54:53', '2024-04-17 18:54:53'),
(38, 6, '2024-04-17 18:54:57', '2024-04-17 18:54:57'),
(39, 5, '2024-04-17 18:55:02', '2024-04-17 18:55:02'),
(40, 4, '2024-04-17 18:55:53', '2024-04-17 18:55:53'),
(41, 9, '2024-04-17 18:56:02', '2024-04-17 18:56:02'),
(42, 5, '2024-04-17 18:56:17', '2024-04-17 18:56:17'),
(43, 31, '2024-04-17 18:56:54', '2024-04-17 18:56:54'),
(44, 33, '2024-04-17 19:07:08', '2024-04-17 19:07:08');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `name`) VALUES
(0, '1.A'),
(1, '1.B'),
(2, '2.A'),
(3, '2.B');

-- --------------------------------------------------------

--
-- Table structure for table `trainer`
--

CREATE TABLE `trainer` (
  `id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trainer`
--

INSERT INTO `trainer` (`id`, `firstname`, `lastname`, `user_id`) VALUES
(1, 'Harry', 'Potter', 25),
(3, 'Julius', 'Satisnký', 26);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `role_id`) VALUES
(22, 'karel_capek123', '$2y$10$3FR2xnn1Zcev8I394xz7xeuX/VHh6KL48xYxi.T74JtauuvTIr6km', 'karel.capek@gmail.com', 1),
(23, 'Uhorka78', '$2y$10$kN9MO1Gv5vkycwjBkJf.MuCQvexxoP8p8x2Ed4ZjObKEqtTlerR0u', 'uhorka.zelena@gmail.com', 1),
(24, 'JanKsenak', '$2y$10$2hWyQF2lxaCMa9njpTRzKOiC91sk61gXFbwcMMoidRCkc6ZDWElQC', 'jan.ksenak@gmail.com', 0),
(25, 'HarryPotter', '$2y$10$UvhiKEP4PAFG0fEoum8kEuKoxkHkBXdc43fEzU/JVITAAFIKFrZI2', 'harry.potter@bradavice.com', 2),
(26, 'JuliusSatinsky', '$2y$10$997rjWWo.psskqNjSPvH4eZ7C4sIDKMRd.8ENv0dbSXoRtWPHy/3a', 'julius.satisnky@gmail.com', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `name`) VALUES
(0, 'admin'),
(2, 'trainer'),
(1, 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `client_has_reservation`
--
ALTER TABLE `client_has_reservation`
  ADD UNIQUE KEY `reservation_id` (`reservation_id`),
  ADD KEY `fk_client_has_reservation_client` (`client_id`);

--
-- Indexes for table `lection`
--
ALTER TABLE `lection`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lection_room` (`room_id`),
  ADD KEY `fk_lection_trainer` (`trainer_id`),
  ADD KEY `fk_lection_type` (`type`) USING BTREE;

--
-- Indexes for table `lection_type`
--
ALTER TABLE `lection_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_lection_reservation` (`lection_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `trainer`
--
ALTER TABLE `trainer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password` (`password`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `fk_user_user_role` (`role_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `lection`
--
ALTER TABLE `lection`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `lection_type`
--
ALTER TABLE `lection_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `trainer`
--
ALTER TABLE `trainer`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fk_client_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `client_has_reservation`
--
ALTER TABLE `client_has_reservation`
  ADD CONSTRAINT `fk_client_has_reservation_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `fk_client_has_reservation_reservation` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`);

--
-- Constraints for table `lection`
--
ALTER TABLE `lection`
  ADD CONSTRAINT `fk_lection_room` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`),
  ADD CONSTRAINT `fk_lection_trainer` FOREIGN KEY (`trainer_id`) REFERENCES `trainer` (`id`),
  ADD CONSTRAINT `fk_lection_tyoe` FOREIGN KEY (`type`) REFERENCES `lection_type` (`id`);

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_lection_reservation` FOREIGN KEY (`lection_id`) REFERENCES `lection` (`id`);

--
-- Constraints for table `trainer`
--
ALTER TABLE `trainer`
  ADD CONSTRAINT `fk_trainer_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_user_role` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
