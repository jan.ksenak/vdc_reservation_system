import './assets/output.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import axios from 'axios'
import VueAxios from 'vue-axios'
import dayjs from 'dayjs'
const app = createApp(App)

app.use(router)
app.use(VueAxios, axios)
//axios.defaults.baseURL = 'http://localhost/VDC/reservation_app/backend/index.php/'
axios.defaults.baseURL = 'http://mrkev.fme.vutbr.cz/~m217372/projekt/backend/index.php/'
axios.interceptors.response.use(response => {
    
    return response;
 }, error => {
   if (error.response.status === 401) {
    console.log(response)
    localStorage.clear()
    router.push('login')
   }
   return error;
 });

app.provide('axios', app.config.globalProperties.axios)  // provide 'axios'
app.provide('dayjs', dayjs)  // provide 'dayjs'
app.mount('#app')
