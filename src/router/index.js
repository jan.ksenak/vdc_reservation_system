import { createRouter, createWebHistory, useRouter } from 'vue-router'

import HomeView from '../views/HomeView.vue'
import LoginView from '@/views/LoginView.vue'
import NotFoundView from '@/views/NotFoundView.vue'
import ProfileView from '@/views/ProfileView.vue'
import RegisterView from '@/views/RegisterView.vue'
import LectionsView from '@/views/LectionsView.vue'
import ReservationView from '@/views/ReservationView.vue'
import axios from 'axios'


// function hasUserAuth(to){
//   const router = useRouter()
//   if(localStorage.getItem("username") === to.params.user){
//     return true
//   }else{
//     router.push('404')
//   }
// }

// function isClient(){
//   const router = useRouter()
//   if(localStorage.getItem("role") === '1'){
//     return true
//   }else{
//     router.push('404')
//   }
// }

// function isTrainer(){
//   const router = useRouter()
//   if(localStorage.getItem("role") === '2'){
//     return true
//   }else{
//     router.push('404')
//   }
// }

// function isAdmin(to){
//   const router = useRouter()
//   if(localStorage.getItem("role") === '0'){
//     return true
//   }else{
//     router.push('404')
//   }
// }



const router = createRouter({
  history: createWebHistory('/~m217372/projekt/'),
  base : '/~m217372/projekt/',
  routes: [
    {
      path: '/:user',
      name: 'home',
      component: HomeView,
      meta:{
        requiresAuth:true
      },
      // beforeEnter: [hasUserAuth]
        
      
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      meta:{
        requiresAuth:false
      }
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView,
      meta:{
        requiresAuth:false
      }
    },
    {
      path: '/404',
      name: '404',
      component: NotFoundView,
      meta:{
        requiresAuth:false
      }
    },
    {
      path: '/:user/lections',
      name: 'lections',
      component: LectionsView,
      meta:{
        requiresAuth:true
      },
      // beforeEnter: [hasUserAuth]
    },
    {
      path: '/:user/reservations',
      name: 'reservations',
      component: ReservationView,
      meta:{
        requiresAuth:true
      },
      // beforeEnter: [hasUserAuth]
    }
    
    
  ]
})




router.beforeEach( (to,from,next)=>{
  
  if(to.matched.some((record)=> record.meta.requiresAuth)){
    const isAuthenticated = localStorage.getItem('jwt')
    if(to.name!=='login' && !isAuthenticated){
      next('login')
    }else{
      next()
     
    }
    }else{
      next()
     
    }
    

})

export default router
